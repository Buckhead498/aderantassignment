﻿using System.Collections.Generic;
using NUnit.Framework;
using AderantAssignment.Library;

namespace AderantAssignment.Tests
{
    [TestFixture]
    public class ShotgunTests
    {
        [Test]
        public void Fire_OneString_ReturnString()
        {
            var inputList = new List<string> {"Return"};
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("Return", returnValue);
        }

        [Test]
        public void Fire_TwoDifferentString_ReturnConcat()
        {
            var inputList = new List<string>
            {
                "ABC",
                "XYZ"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("ABCXYZ", returnValue);
        }

        [Test]
        public void Fire_ThreeDifferentStrings_ReturnConcat()
        {
            var inputList = new List<string>
            {
                "CD",
                "EF",
                "AB"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("ABCDEF", returnValue);
        }

        [Test]
        public void Fire_TwoIdenticalStrings_ReturnString()
        {
            var inputList = new List<string>
            {
                "ABC",
                "ABC"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("ABC", returnValue);
        }

        [TestCase("ABCD", "BCDE", 3)]
        [TestCase("ABC", "DEF", 0)]
        [TestCase("ABC","BCD",2)]
        [TestCase("ABC", "ABC", 3)]
        [TestCase("ds well and now", "d now a", 5)]
        public void Compare_OverlapStrings_ReturnSizeOfOverlap(string front, string back, int expected)
        {
            var shotgun = new GenomeShotgun(new List<string>());

            var match = shotgun.Compare(front, back);

            Assert.AreEqual(expected, match.Size);
        }

        [Test]
        public void Fire_OverlapString_ReturnString()
        {
            var inputList = new List<string>
            {
                "ABC",
                "BCD"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("ABCD", returnValue);
        }

        [Test]
        public void Fire_BestOverlapIsSecond_ReturnString()
        {
            var inputList = new List<string>
            {
                "ABC",
                "DEF",
                "EFG"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("ABCDEFG", returnValue);
        }

        [Test]
        public void Fire_ExamplePhrase_ReturnString()
        {
            var inputList = new List<string>
            {
                "all is well",
                "ell that en",
                "hat end",
                "t ends well"
            };
            var shotgun = new GenomeShotgun(inputList);

            var returnValue = shotgun.Fire();

            Assert.AreEqual("all is well that ends well", returnValue);
        }

        [Test]
        public void MatchingOnAllCriteriaCombined()
        {
            var fragments = new List<string>
            {
                "abxy",
                "ell that en",
                "xyab",
                "t ends well",
                "hat end",
                "ell",
                "d now a",
                "ow a simple t",
                "ds well and now",
                "all is well",
                "a simple test"
            };

            var result = new GenomeShotgun(fragments).Fire();
            
            Assert.IsTrue(result.Equals("abxyaball is well that ends well and now a simple test") ||
                          result.Equals("xyabxyall is well that ends well and now a simple test") ||
                          result.Equals("all is well that ends well and now a simple testabxyab") ||
                          result.Equals("all is well that ends well and now a simple testxyabxy"));
        }
    }
}
