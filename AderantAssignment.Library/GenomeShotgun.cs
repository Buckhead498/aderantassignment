﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AderantAssignment.Library
{
    public class GenomeShotgun
    {
        private List<string> inputList;
        private List<Match> matchList;

        public GenomeShotgun(List<string> inputList)
        {
            this.inputList = inputList;
        }

        public string Fire()
        {
            RemoveDuplicates();
            while (inputList.Count > 1)
            {
                matchList = new List<Match>();
                FindBestMatch();
                MakeBestMatch();
            }
            return inputList.First();
        }

        private void RemoveDuplicates()
        {
            inputList = inputList.Distinct().ToList();
        }

        private void FindBestMatch()
        {
            foreach (var currentLeft in inputList)
            {
                foreach (var currentRight in inputList.Where(i => i != currentLeft))
                {
                    matchList.Add(Compare(currentLeft, currentRight));
                }
            }
            
        }

        private void MakeBestMatch()
        {
            var bestMatch = matchList.OrderByDescending(m => m.Size).First();
            inputList.Remove(bestMatch.Left);
            inputList.Remove(bestMatch.Right);
            inputList.Add(CombineString(bestMatch));
        }

        private string CombineString(Match match)
        {
            return match.Left + match.Right.Substring(match.Size);
        }

        public Match Compare(string front, string back)
        {
            var maxLength = front.Length > back.Length ? back.Length : front.Length;
            for(var i = 0; i < front.Length; i++)
            {
                var temp = i + maxLength - front.Length;
                var offset = temp > 0 ? temp : 0;
                var frontBit = front.Substring(i, maxLength - offset);
                var backBit = back.Substring(0, maxLength - offset);
                if (frontBit.Equals(backBit))
                    return new Match(front, back, frontBit.Length, i);
            }

            return new Match(front, back, 0, front.Length);
        }
    }

    public struct Match
    {
        public string Left { get; }
        public string Right { get; }
        public int Size { get; }
        public int Position { get; }

        public Match(string left, string right, int size, int position)
        {
            Left = left;
            Right = right;
            Size = size;
            Position = position;
        }
    }
}
